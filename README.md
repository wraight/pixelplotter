# PixelPlotter

Python2[^1] based code to convert pixel data to imageJ compatible format (with option to show map).

[^1]: may work with python3, not tried

## Requirements
* matplotlib
* numpy
* time
* argparse

## Usage

**Command**
`pixelPlotter.py`

| Args | Comment (default) | e.g. |
| --- | --- | --- |
| infile | input filename (not set) |  2D_map_127_127.dat |
| outfile | oiutput filename (will generate from infile) |  converted_2D_map_127_127.dat |
| plot | showplots | 1 |

*E.g.*
> python pixelPlotter.py --infile 2D_map_127_127.dat --plot 1 --outfile converted_2D_map_127_127

**_Comments_**
