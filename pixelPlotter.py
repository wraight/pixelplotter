import matplotlib.pyplot as plt
import numpy
import time
import argparse

# commandline arguments
# specify input filename output filename
# flag show image

######################
### parsing
######################

def GetArgs():
    parser = argparse.ArgumentParser(description=__file__)

    # pass on
    parser.add_argument('--infile', help='input filename')
    parser.add_argument('--outfile', help='output filename')
    parser.add_argument('--plot', help='show plots')

    args = parser.parse_args()

    print "args:",args

    argDict={'infile':"NYS", 'outfile': "NYS", 'plot':1 }

    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]


    return argDict

######################################
### useful functions
######################################

def File2Array(filename, pos, delim="\t"):
#read file and retuen x,y,c array
    f= open(filename,mode='r')

    retArr=[]
    for line in f.readlines():
        arr = line.split(delim)
        #print(arr)
        retArr.append(''.join(i for i in arr[pos] if (i.isdigit() or i=='-')))

    f.close()

    return retArr

def SpecNewArray(oldArr, neg=1):
#gleen max, min and minimum distance (width) values
    width=1e10
    max=-1e10
    min=1e10

    last=-1
    for sa in oldArr:
        a=float(sa) #*neg
        if a>max: max=a
        if a<min: min=a
        if abs(last-a)<width and a!=last: width=abs(last-a)
        last=a
    '''
    if neg<0:
        temp=max
        max=min*neg
        min=temp*neg
        '''
    size=(max-min)/width
    print("size:",size)
    return max, min, width

def Reordinate(val, max, min, width):
# derive new coordinate from position based on max, min and width
    return int( (float(val)-min)/width )

def Write2File(filename,arr):

    numpy.savetxt(filename, arr, delimiter=',')

######################
### main
######################

def main():

    argDict=GetArgs()
    print "argDict:",argDict

    if "NYS" in argDict['infile']:
        print("No input filename specificed. Exiting")
        return

    arrX=File2Array(argDict['infile'],1)
    arrY=File2Array(argDict['infile'],2)
    arrC=File2Array(argDict['infile'],3)
    #print(arrC)

    maxX, minX, widthX = SpecNewArray(arrX)
    sizeX=int((maxX-minX)/widthX)
    print("X... ",maxX, minX, widthX, sizeX)

    maxY, minY, widthY = SpecNewArray(arrY)
    sizeY=int((maxY-minY)/widthY)
    print("Y... ",maxY, minY, widthY, sizeY)

    newArr=numpy.zeros((sizeX, sizeY))
    for x,y,c in zip(arrX,arrY,arrC):
        newX=int(Reordinate(x,maxX, minX, widthX))
        newY=int(Reordinate(y,maxY, minY, widthY))
        #print(x,y)
        #print(newX,newY)
        newArr[newX-1][newY-1]=float(c)

    if argDict['plot']==1:
        #print(newArr)
        plt.imshow(newArr, aspect = 'auto')
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.title('response')
        plt.show()

    outfilename=argDict['outfile']
    if "NYS" in outfilename:
        outfilename="converted_"+argDict['infile']
        print("No output filename specificed, using: ",outfilename)
    Write2File(outfilename,newArr)


if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "\n+++ Total scan time: ",(end-start),"seconds +++\n"
    print "### out",__file__,"###"

'''
plt.plot([1, 2, 3, 4])
plt.ylabel('some numbers')
plt.show()
'''
